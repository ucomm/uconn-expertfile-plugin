(function() {

  var embedNodeList = document.querySelectorAll("iframe[id^='embed-frame']");

  embedNodeList.forEach(function (embed, index, node) {

    var embed = 'embed-' + index;
    var SF = SF || { index: index };

    SF[embed] = node[index];

    var seamless = new SeamLess({
      window: SF[embed].contentWindow,
      origin: '*',
      frameId: node[index].id
    });

    seamless.receiveHeight({
      channel: 'all'
    }, function (height) {
      SF[embed].style.height = height + 'px';
      SF[embed].style.overflowX = 'hidden';

    });

  });
})();
