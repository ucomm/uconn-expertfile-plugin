<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    UConn_Expertfile_Plugin
 * @subpackage UConn_Expertfile_Plugin/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    UConn_Expertfile_Plugin
 * @subpackage UConn_Expertfile_Plugin/includes
 * @author     Your Name <adam.berkowitz@uconn.edu>
 */
class UConn_Expertfile_Plugin {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      UConn_Expertfile_Plugin_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $UConn_Expertfile_Plugin    The string used to uniquely identify this plugin.
	 */
	protected $UConn_Expertfile_Plugin;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'uconn-expertfile-plugin';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		// $this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_public_settings();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - UConn_Expertfile_Plugin_Loader. Orchestrates the hooks of the plugin.
	 * - UConn_Expertfile_Plugin_i18n. Defines internationalization functionality.
	 * - UConn_Expertfile_Plugin_Admin. Defines all hooks for the admin area.
	 * - UConn_Expertfile_Plugin_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-uconn-expertfile-plugin-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-uconn-expertfile-plugin-i18n.php';



		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-uconn-expertfile-plugin-view.php';




		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		// require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-uconn-expertfile-plugin-admin.php';

		/**
		*
		*	The class responsible for handling the plugin admin settings page.
		*/
		require_once plugin_dir_path(dirname(__FILE__)) . 'settings/class-uconn-expertfile-plugin-settings.php';

		/**
		*
		*	The class responsible for coordinating data and classes for the plugin
		*/
		require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-uconn-expertfile-plugin-controller.php';

		$this->loader = new UConn_Expertfile_Plugin_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the UConn_Expertfile_Plugin_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new UConn_Expertfile_Plugin_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	// private function define_admin_hooks() {

	// 	$plugin_admin = new UConn_Expertfile_Plugin_Admin( $this->get_UConn_Expertfile_Plugin(), $this->get_version() );

	// 	$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
	// 	$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

	// }

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_controller = new UConn_Expertfile_Plugin_Controller( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_controller, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_controller, 'enqueue_scripts' );
		$this->loader->add_shortcode('expertfile', $plugin_controller, 'uconn_expert_shortcode_handler');
		$this->loader->add_shortcode('expertfile-spotlight', $plugin_controller, 'uconn_expert_shortcode_handler');

	}

	private function define_public_settings() {
		$plugin_settings = new UConn_Expertfile_Plugin_settings($this->get_plugin_name(), $this->get_version());
		$this->loader->add_action('admin_menu', $plugin_settings, 'add_settings');
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    UConn_Expertfile_Plugin_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
