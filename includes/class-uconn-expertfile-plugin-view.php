<?php
/*
*
* This class will manage the view for the plugin.
* For now it will just handle iframes provided by expertfile.
* If needed it can be modified to use the expertfile API.
*/


class UConn_Expertfile_View {
  // The shortcode_atts array provided by the controller.
  private $atts_array;
  public function __construct($atts_array, $tag) {
    $this->atts_array = $atts_array;
    $this->html = '';
    $this->org = '5517';
    $this->tag = $tag;
  }
  public function wp() {
    global $wp;
    return $wp;
  }
  public function get_view() {
    if ($this->tag === 'expertfile') {
      $this->get_profile();
    } elseif ($this->tag === 'expertfile-spotlight') {
      $this->get_spotlight();
    }
  }
  public function get_profile() {
    $wp = $this->wp();
		$current_page = home_url(add_query_arg(array(), $wp->request));

    $content = 'name,title,headline,expertise';

    if ($this->atts_array['phone']) {
      $content .= ',phone';
    }
    if ($this->atts_array['email']) {
      $content .= ',email';
    }

    if (isset($_GET['expert'])) {

			$expert = $_GET['expert'];
			$this->html = "<iframe id='embed-frame-featured' class='embed_preview profile' frameborder='0' ";
			$this->html .= "scrolling='" . $this->atts_array['scrolling'] . "' style='border: none; width: 100%;' height='5000' src='//embed.expertfile.com/v1/expert/$expert/1?access=public&powered_by=no&avatar=square&expert=$expert&url_override=$current_page/?inquiry=$expert&open_tab=no'></iframe>";

    } elseif (isset($_GET['inquiry'])) {
			$expert = $_GET['inquiry'];
			$this->html = "<iframe id='embed-frame-inquiry' class='embed_preview profile' frameborder='0' ";
			$this->html .= "scrolling='" . $this->atts_array['scrolling'] . "' style='border: none; width: 100%;' height='1000' src='//embed.expertfile.com/v1/inquiry/$expert/1?inquiry=$expert&access=public&avatar=square&powered_by=no&open_tab=no'></iframe>";

		} elseif (!isset($_GET['spotlight'])) {
			$this->html = "<iframe id='embed-frame-directory' class='embed_preview profile' frameborder='0' ";
			$this->html .= "scrolling='" . $this->atts_array['scrolling'] . "' style='border: none; width: 100%;' height='800' src='//embed.expertfile.com/v1/organization/$this->org/2?access=public&powered_by=no&avatar=square&content=$content&url_override=$current_page/?expert={{username}}&open_tab=no'></iframe>";
		}
    echo $this->html;
  }
  public function get_spotlight() {
    $wp = $this->wp();
    $current_page = home_url(add_query_arg(array(), $wp->request));
    if (isset($_GET['spotlight'])) {
      $spotlight = $_GET['spotlight'];
      $this->html = "<iframe id='embed-frame-spotlight' class='embed_preview spotlight' frameborder='0' scrolling='" . $this->atts_array['scrolling']  . "' style='border: none; width: 100%;' height='1500' src='//embed.expertfile.com/v1/spotlight/$this->org/$spotlight/1?content=name&hide_search_bar=yes&hide_search_category=yes&hide_search_sort=yes&url_override=$current_page/?expert={{username}}&open_tab=no&powered_by=no'></iframe>";
    } else {
      $this->html = "<iframe id='embed-frame-spotlight' class='embed_preview spotlight' height='300' src='//embed.expertfile.com/v1/spotlight/$this->org/2?num_post=" . $this->atts_array['num_post'] . "&content=name&hide_search_bar=yes&hide_search_category=yes&hide_search_sort=yes&url_override=$current_page/?spotlight={{postid}}&open_tab=no&avatar=circle&powered_by=no' style='border: none; width: 100%;' width='300'></iframe>";
    }
    echo $this->html;
  }
}