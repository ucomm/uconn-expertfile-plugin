# Changelog
## 1.1.1
- Switched iframe so only public profiles display
## 1.1.0
- Added second shortcode for displaying spotlight stories.
## 1.0.2
- Only public profiles will display
## 1.0.1
- Enqueues assets only on pages that have the shortcode
## 1.0.0
- Initial release