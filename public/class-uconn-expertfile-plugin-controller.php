<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    UConn_Expertfile_Plugin
 * @subpackage UConn_Expertfile_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    UConn_Expertfile_Plugin
 * @subpackage UConn_Expertfile_Plugin/public
 * @author     Your Name <email@example.com>
 */
class UConn_Expertfile_Plugin_Controller {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $UConn_Expertfile_Plugin    The ID of this plugin.
	 */
	private $UConn_Expertfile_Plugin;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $UConn_Expertfile_Plugin       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
    $this->scrolling = 'no';
    $this->phone = '';
		$this->email = '';
		$this->num_post = '6';
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		global $post;
		if (has_shortcode($post->post_content, 'expertfile')) {
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/uconn-expertfile-public.css', array(), $this->version, 'all' );
		}
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		global $post;
		$expertfile_shortcode = has_shortcode($post->post_content, 'expertfile');
		$spotlight_shortcode = has_shortcode($post->post_content, 'expertfile-spotlight');

		if ($expertfile_shortcode || $spotlight_shortcode) {
			wp_enqueue_script('expertfile-js', '//d2mo5pjlwftw8w.cloudfront.net/embed/seamless.ly.min.v1.0.4.js', array(), $this->version, false);
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/uconn-expertfile-public.js', array( 'jquery' ), $this->version, false );
		}
		
	}
  public function uconn_expert_shortcode_handler($atts = array(), $content = null, $tag = '') {

		$atts_array = null;
		$shortcode_atts = array_change_key_case((array)$atts, CASE_LOWER);

		if ($tag === 'expertfile') {
			$atts_array = array(
				'scrolling' => $this->scrolling,
				'phone' => $this->phone,
				'email' => $this->email
			);
		} else {
			$atts_array = array(
				'scrolling' => $this->scrolling,
				'num_post' => $this->num_post
			);
		}

		$shortcode_atts = shortcode_atts($atts_array, $atts, $tag);

    $output = ob_start();
    echo $this->load_view($shortcode_atts, $tag);
		$output = ob_get_clean();
    return $output;

  }
  private function load_view($atts_array, $tag) {
		$view = new UConn_Expertfile_View($atts_array, $tag);
		$view->get_view();
  }
}