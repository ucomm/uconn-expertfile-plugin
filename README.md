# Expertfile Plugin
This plugin will (initially) be used to display the iframes provided by [Expertfile](https://expertfile.com/). It may be expanded in the future to use their [API](http://developer.expertfile.com/#authentication) to provide additional functionality, customization, or refinements.

## Usage
### Getting started

NB - If you anticipate needing to use a plugin which needs access to the `vendor` directory (like Castor), map it as follow. In `docker-compose.yml`, find the `web` image and under `volumes` write `- ./vendor:/var/www/html/content/plugins/{plugin_name}/vendor`. This gives both the current theme and plugin the vendor folder.

### To get a project running.
```bash
$ composer install # first time only
$ docker-compose up
```
### Accessing containers
To access a particular docker container, find the container name and then enter an interactive terminal.
```bash
$ docker ps # to get the container name
$ docker exec -it container_name /bin/bash
```
### Debugging Wordpress
Wordpress debug logs can be found inside the web container at `/etc/httpd/logs/error_log`

## Bitbucket
### Creating releases
Assuming you're using git flow, tag the release with the command `git flow release start {version_number}`. Tags must follow the [semver system](http://semver.org/). Follow these steps to complete a release
```bash
git tag # check the current tags/versions on the project
git flow release start {new_tag}
git flow release finish {new_tag}
# in the first screen add new info or just save the commit message as is
# in the second screen type in the same tag you just used and save.
git push --tags && git push origin master
git checkout develop
``` 
Finally re-run the pipeline build on the [satis repo](https://bitbucket.org/ucomm/composer-repository).
### Pipelines
This repo has a bitbucket pipelines (written by Adam Berkowitz) attached in case you wish to create zip file downloads for tags/branches. **You may exclude files/folders from the zip archive by adding them to composer.json. The syntax is the same as gitignore.** To enable pipelines on bitbucket, simply visit the project repo and enable pipelines at `repo -> settings -> Pipelines/settings`.

## Using wp-project-boilerplate for a plugin

It would be a good idea to keep UComm/WordPress dependencies in require-dev, and only keep functional dependencies in "require".  That way, your package will export with only the required files for your plugin/theme to function, and not include a full WP install. See the [Castor plugin](https://bitbucket.org/ucomm/castor/src) for an example that uses both.

## Known Issues
- The Docker images don't currently work under Linux. This is an issue related to ip tables.
- ~~Docker does not work. Need a working image for WordPress.~~
- ~~`www/wp-config.php` needs to be edited accordingly.~~
- After a release/version bump re-run the [Composer Repository Pipelines build](https://bitbucket.org/ucomm/composer-repository).
