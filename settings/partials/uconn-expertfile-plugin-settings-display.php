<h1>UConn Expertfile Plugin Settings</h1>
<div>
  <h2>Overview</h2>
  <p>This plugin exposes two shortcodes which can be placed on any page. Both shortcodes can be used on a single page or on different pages.</p> 
  <h3>Profile Shortcode</h3>
  <p>The - <code>[expertfile]</code> shortcode will give users access to the following workflow:</p>
  <ul>
    <li>- Directory of experts</li>
    <li>- Profile of a single expert</li>
    <li>- Contact form for a single expert</li>
  </ul>
  <p>Currently there are only two settings for this shortcode. Simply add <code>phone="yes"</code> or <code>email="yes"</code> to display phone numbers and email addresses for experts. Please note that <code>phone</code> doesn't currently work. It is an issue with Expertfile.</p>
  <h3>Spotlight Shortcode</h3>
  <p>The - <code>[expertfile-spotlight]</code> shortcode will display spotlights created from articles within expertfile. The workflow for users will be:
  <ul>
    <li>- A list of the spotlights will be displayed</li>
    <li>- When a user clicks on the story, they will be taken to the article page</li>
    <li>- If an expert is attached to the story, users will be able to click on the expert and go to their profile and contact pages.</li>
    <li><strong>- NB: Please note if you add both shortcodes to a page and a user clicks into a spotlight story, they won't see the expert directory until they return to the starting page.</strong></li>
  </ul> 
  <p>This shortcode has one option:</p>
  <ul>
    <li>- <code>num_post</code> the number of articles to display</li>
  </ul>
  <hr />
  <h2>Examples</h2>
  <ul>
    <li>
      <code>[expertfile]</code>
    </li>
    <li>
      <code>[expertfile phone="yes"]</code>
    </li>
    <li>
      <code>[expertfile email="yes"]</code>
    </li>
    <li>
      <code>[expertfile phone="yes" email="yes"]</code>
    </li>
    <li>
      <code>[expertfile-spotlight]</code>
      <p>This will display the default 6 posts.</p>
    </li>
    <li>
      <code>[expertfile-spotlight num_post="3"]</code>
      <p>This will display 3 posts.</p>
    </li>
  </ul>
</div>