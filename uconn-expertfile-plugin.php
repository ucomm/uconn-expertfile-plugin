<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * and defines a function that starts the plugin.
 *
 * @link              TODO
 * @since             1.0.0
 * @package           UConn_Expertfile_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       UConn Expertfile Plugin
 * Description:       Displays experts from expertfile.com
 * Version:           1.1.1
 * Author:            Adam Berkowitz/UComm Webteam
 * Author URI:        TODO
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       uconn-expertfile-plugin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-uconn-expertfile-plugin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_uconn_expertfile_plugin() {

	$plugin = new UConn_Expertfile_Plugin();
	$plugin->run();

}
run_uconn_expertfile_plugin();
