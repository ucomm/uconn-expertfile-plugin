<?php

/**
 * The settings page functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    UConn_Expertfile_Plugin
 * @subpackage UConn_Expertfile_Plugin/public
 */

/**
 * The settings page functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    UConn_Expertfile_Plugin
 * @subpackage UConn_Expertfile_Plugin/public
 * @author     Adam Berkowitz/UComm web team
 */
class UConn_Expertfile_Plugin_Settings {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	* Adds the sub-menu item and display of the plugin documentation
	* to the settings section of the dashboard.
	*
	* @since 	1.0.0
	*/
	public function add_settings(){

		/**
		* Hook to create the settings page for the documentation.
		* Documentaton HTML is viewed in the partials subdirectory in settings.
		*/
		add_options_page( 'UConn Expertfile Options', 'UConn Expertfile Plugin', 'manage_options', 'uconn_expertfile_settings', array( $this, 'settings_page' ) );
	
	}

	/**
	* The callback function for settings display.
	*
	* @since 	1.0.0
	*/
	public function settings_page(){

		ob_start();
		include( plugin_dir_path( __FILE__ ) . 'partials/uconn-expertfile-plugin-settings-display.php' );
		echo ob_get_clean();
		
	}

}